
import sys

# add more table morse right side  for example from 
# https://www.electronics-notes.com/articles/ham_radio/morse_code/characters-table-chart.php
morse = {
        'A' : '.-',
        'B' : '-...',
        'C' : '-.-.',
        'D' : '-..',
        'E' : '.',
        'F' : '..-.',
        'G' : '--.',
        'H' : '....',
        'I' : '..',
        'J' : '.---',
        'K' : '-.-',
        'L' : '.-..',
        'M' : '--',
        'N' : '-.',
        'O' : '---',
        'P' : '.--.',
        'Q' : '--.-',
        'R' : '.-.',
        'S' : '...',
        'T' : '-',
        'U' : '..-',
        'V' : '...-',
        'W' : '.--',
        'X' : '-..-',
        'Y' : '-.--',
        'Z' : '--..'
        }

def decrypt(arg):
    
    def get_key(val):
        for dec, enc in morse.items():
            if val == enc:
                return dec

    result = ""
    for code in arg.split():
         result += get_key(code)

    return result

def encrypt(arg):
    result = ""
    for code in arg:
        result += (str(morse[code]) + " ")

    return result

if __name__ == '__main__':
    print(decrypt(sys.argv[1]))


