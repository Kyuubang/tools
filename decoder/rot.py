#!/usr/bin/env python
# require python version >= 3

import string
import sys


def rot_function(chiper, rot):
    ascii_lower = string.ascii_lowercase
    ascii_upper = string.ascii_uppercase

    plain_text = ''
    for i in chiper:
        if i in ascii_lower:
            plain_text += ascii_lower[(ascii_lower.index(i) + int(rot)) % 26]
        elif i in ascii_upper:
            plain_text += ascii_upper[(ascii_upper.index(i) + int(rot)) % 26]
        else:
            plain_text += i
    return plain_text

if __name__ == '__main__':
    if len(sys.argv) == 2:
        chiper = sys.argv[1]
        for i in range(26):
            print(rot_function(chiper, i), "rotate at ", str(i))
        print("\nUsage: python3 rot.py 'chiper' <int>")
    elif len(sys.argv) == 3:
        chiper = sys.argv[1]
        rot = sys.argv[2]
        print(rot_function(chiper, rot))
    else :
        print("\nUsage: python3 rot.py 'chiper' <int>")
    #try :
    #    chiper = sys.argv[1]
    #    rot = sys.argv[2]
    #    print(rot_function(chiper, rot))
    #except:
    #    print("Usage : python3 rot.py 'chiper' <int>")
