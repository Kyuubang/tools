#!/usr/bin/python3

import sys
import random

def random_ip():
    octet1  = random.randint(1, 255)
    octet2  = random.randint(0, 255)
    octet3  = random.randint(0, 255)
    octet4  = random.randint(0, 255)
    prefix  = random.randint(8, 30)

    # print ip random
    random_ipv4 = (f'{octet1}.{octet2}.{octet3}.{octet4}/{prefix}')
    print(random_ipv4)

if __name__ == '__main__':
    for i in range(int(sys.argv[1])):
        random_ip()


