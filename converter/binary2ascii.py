#!/usr/bin/python3
#
# -*- coding: utf-8 -*-
#
#     binary_to_ascii.py
#
#     Copyright (C) 2021  Ahmad Bayhaqi <ahmadbayhaqi@nevtik.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import binascii
import re

input_file = open(sys.argv[1], 'r')
text = re.sub(r'[" ",\n]', '', input_file.read())
input_file.close()
# file_to_string = '0b' + str(input_file.read()).replace(' ','')

def parsing_odd_binary(text):
    if len(text) % 8 == 0:
        text = '0b' + text
    else :
        text = '0b' + text[:(len(text) - len(text) % 8)]

    return text
def binary_to_decimal(binary):
    return int(binary, 2)


if __name__ == '__main__':
    file_to_string = parsing_odd_binary(text)
    n = binary_to_decimal(file_to_string)
    print(n.to_bytes((n.bit_length() + 7) // 8, 'big').decode())
